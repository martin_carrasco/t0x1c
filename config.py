#Bot Configs!
user = [""] #Skype username
menu = '''
\t Welcome to P01S0N menu!
\tStart all commands with '!'
\t- !msg <name> <txt> - Send certain msg to certain person based on their skype name
\t- !help- Shows this menu
\t- !spamp <name> <txt> - Spams the person whos username you put as second value
\t- !spamc <txt> -Spams the chat you are currenltly on!
\t- !afk <on/off> <time> - Time is optional. Will send a chat msg saying AFK and mute your mic. 
\t- !gaming <on/off> - Sends AFK Message and puts in Do Not Disturb (Working to combine with !game)
\t- !game <name> - Starts a game from its pathfile
\t- !dildo <name> - Displays a funny msg!
\t- !crypt <word> - Crypts a word in md5 hash
\t- !call <name> - Calls a skype username
\t- !reload - Opens and closes the bot(WARNING: It closes all instances of python.exe and py.exe aswell!)
\t- !translate <on/off> - Turns auto translation On/Off
\t- !clear - Clear Chat history
\t- More features to be added!
'''
changelog = '''
16/11/13-\tSimple msg and help commands added.
16-26/11/13-\tAdded AFK funcction as well as !dildo, !add, !spamc, !spamp and !msg
1-13/12/12-\tAdded !gaming, !game, !clear, !reload, !call, !translate. Refined previous commands.
''' 
help = '''
\t- Type !help to get this menu 
\t- Type !change to get the changelog
\t- Type !menu for the commands
'''
